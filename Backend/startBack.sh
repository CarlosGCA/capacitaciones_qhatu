#!/bin/bash
dot="$(cd "$(dirname "$0")"; pwd)"
echo $dot
cd $dot && (screen -d -m nohup python3 ./app.py &)