#!/bin/bash
dot="$(cd "$(dirname "$0")"; pwd)"
ip=$(dig +short myip.opendns.com @resolver1.opendns.com)
echo $dot
echo $ip
sed -i -e "s/\(REACT_APP_BACK_IP=\).*/\1$ip/" $dot/.env
cd $dot && (npm install)
cd $dot && (npm run start&)