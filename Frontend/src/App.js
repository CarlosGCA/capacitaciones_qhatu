import React, {useState} from "react";
import logo from "./logo.svg";
import "./App.css";
import axios from "axios";

const api = axios.create({
  baseURL: "http://" + process.env.REACT_APP_BACK_IP + ":5000",
});

function cargarmensaje() {
  return api.get("greetings").then((response) => {
    console.log(response)
    return response.data;
  });
}

function App() {
  const [mensaje, setMensaje] = useState("");

  const obtenerMensaje = async ()=>{
    var men = await cargarmensaje()
    setMensaje(men)
  }
  obtenerMensaje()
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>{mensaje}</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
